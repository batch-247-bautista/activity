import { Form, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function Register() {

	// State hooks to store values of teh input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");


	// State to determine whenter button is enabled or not
	const [isActive, setIsActive] = useState(false);


	console.log(email);
	console.log(password2);
	console.log(password1);

	function registerUser(e){

		e.preventDefault();

		setEmail("");
		setPassword2("");
		setPassword1("");

		alert("Thank you for registering!");

	}
	useEffect(() => {

		// Validation to enable submit button when all fields are populatsed and both passwords match

		if((email !=="" && password1 !=="" && password2 !=="") && (password1 === password2)&& (password1.length && password2.length >=8)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);


	return (
		<Form onSubmit ={(e) => registerUser(e)}>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>


			{isActive?

			<Button variant="primary my-3" type="submit" id="submitBtn"> Submit</Button>
			:
			<Button variant="danger my-3" type="submit" id="submitBtn"> Submit</Button>
			}


		</Form>
		)
}


