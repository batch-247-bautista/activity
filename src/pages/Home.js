import Banner from '../components/Banner';
import Highlight from '../components/Highlight';
import Login from '../components/Login';

export default function Home(){
	return (
		<>
			<Banner />
			<Highlight />
			<Login />
		</>
			)
}
