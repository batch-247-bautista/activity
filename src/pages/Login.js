import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login() {

  // State hooks to store values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  function loginUser(e){
    e.preventDefault();
    alert("Login Successful!");
  }

  useEffect(() => {

    // Validation to enable submit button when all fields are populated
    if(email !== "" && password !== ""){
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Form onSubmit={(e) => loginUser(e)}>
    	 <Form.Label style={{ fontSize: '24px' }}>Login</Form.Label>
      <Form.Group controlId="email">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          required
        />
      </Form.Group>

      { isActive ? 
        <Button variant="primary my-3" type="submit" id="submitBtn">Login</Button>
        :
        <Button variant="danger my-3" type="submit" id="submitBtn" disabled>Login</Button>
      }

    </Form>
  )
}
